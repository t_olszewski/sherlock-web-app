﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sherlock.Data.Models;

namespace Sherlock.Data
{
    public interface IDataRepository
    {
        // Projects

        Task<Project> Project(long id);
        Task<string> ProjectAppName(long id);
        IEnumerable<Project> ProjectsDropDownList();

        // Stories

        Task<Story> Story(long id);

        // FAQs

        Task<FAQ> FAQ(long id);

        // Tags

        Task<Tag> Tag(long id);
        IEnumerable<Tag> TagsDropDownList(long projectID);

        // Integrations

        Task<Integration> Integration(long id);

        // Roles

        IEnumerable<IdentityRole> RolesDropDownList();
    }
}
