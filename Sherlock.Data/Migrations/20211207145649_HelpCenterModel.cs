﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class HelpCenterModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HelpCenterID",
                table: "FAQ",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HelpCenter",
                columns: table => new
                {
                    HelpCenterID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectID = table.Column<int>(type: "int", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HelpCenter", x => x.HelpCenterID);
                    table.ForeignKey(
                        name: "FK_HelpCenter_Project_ProjectID",
                        column: x => x.ProjectID,
                        principalTable: "Project",
                        principalColumn: "ProjectID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FAQ_HelpCenterID",
                table: "FAQ",
                column: "HelpCenterID");

            migrationBuilder.CreateIndex(
                name: "IX_HelpCenter_ProjectID",
                table: "HelpCenter",
                column: "ProjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_FAQ_HelpCenter_HelpCenterID",
                table: "FAQ",
                column: "HelpCenterID",
                principalTable: "HelpCenter",
                principalColumn: "HelpCenterID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FAQ_HelpCenter_HelpCenterID",
                table: "FAQ");

            migrationBuilder.DropTable(
                name: "HelpCenter");

            migrationBuilder.DropIndex(
                name: "IX_FAQ_HelpCenterID",
                table: "FAQ");

            migrationBuilder.DropColumn(
                name: "HelpCenterID",
                table: "FAQ");
        }
    }
}
