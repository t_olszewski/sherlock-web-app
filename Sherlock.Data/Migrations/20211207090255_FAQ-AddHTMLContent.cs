﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class FAQAddHTMLContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HTMLContent",
                table: "FAQ",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HTMLContent",
                table: "FAQ");
        }
    }
}
