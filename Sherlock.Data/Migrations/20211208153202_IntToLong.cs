﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class IntToLong : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Story_StoryID",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQ_FAQ_FAQID1",
                table: "FAQ");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQ_HelpCenter_HelpCenterID",
                table: "FAQ");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQ_Project_ProjectID",
                table: "FAQ");

            migrationBuilder.DropForeignKey(
                name: "FK_HelpCenter_Project_ProjectID",
                table: "HelpCenter");

            migrationBuilder.DropForeignKey(
                name: "FK_Integration_Project_ProjectID",
                table: "Integration");

            migrationBuilder.DropForeignKey(
                name: "FK_Story_Project_ProjectID",
                table: "Story");

            migrationBuilder.DropForeignKey(
                name: "FK_Tag_Project_ProjectID",
                table: "Tag");

            migrationBuilder.DropForeignKey(
                name: "FK_TagFAQ_FAQ_FAQID",
                table: "TagFAQ");

            migrationBuilder.DropForeignKey(
                name: "FK_TagFAQ_Tag_TagID",
                table: "TagFAQ");

            migrationBuilder.DropForeignKey(
                name: "FK_TagStory_Story_StoryID",
                table: "TagStory");

            migrationBuilder.DropForeignKey(
                name: "FK_TagStory_Tag_TagID",
                table: "TagStory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TagStory",
                table: "TagStory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TagFAQ",
                table: "TagFAQ");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tag",
                table: "Tag");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Story",
                table: "Story");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Project",
                table: "Project");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Integration",
                table: "Integration");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HelpCenter",
                table: "HelpCenter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FAQ",
                table: "FAQ");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comment",
                table: "Comment");

            migrationBuilder.RenameTable(
                name: "TagStory",
                newName: "TagStories");

            migrationBuilder.RenameTable(
                name: "TagFAQ",
                newName: "TagFAQs");

            migrationBuilder.RenameTable(
                name: "Tag",
                newName: "Tags");

            migrationBuilder.RenameTable(
                name: "Story",
                newName: "Stories");

            migrationBuilder.RenameTable(
                name: "Project",
                newName: "Projects");

            migrationBuilder.RenameTable(
                name: "Integration",
                newName: "Integrations");

            migrationBuilder.RenameTable(
                name: "HelpCenter",
                newName: "HelpCenters");

            migrationBuilder.RenameTable(
                name: "FAQ",
                newName: "FAQs");

            migrationBuilder.RenameTable(
                name: "Comment",
                newName: "Comments");

            migrationBuilder.RenameIndex(
                name: "IX_TagStory_TagID",
                table: "TagStories",
                newName: "IX_TagStories_TagID");

            migrationBuilder.RenameIndex(
                name: "IX_TagStory_StoryID",
                table: "TagStories",
                newName: "IX_TagStories_StoryID");

            migrationBuilder.RenameIndex(
                name: "IX_TagFAQ_TagID",
                table: "TagFAQs",
                newName: "IX_TagFAQs_TagID");

            migrationBuilder.RenameIndex(
                name: "IX_TagFAQ_FAQID",
                table: "TagFAQs",
                newName: "IX_TagFAQs_FAQID");

            migrationBuilder.RenameIndex(
                name: "IX_Tag_ProjectID",
                table: "Tags",
                newName: "IX_Tags_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_Story_ProjectID",
                table: "Stories",
                newName: "IX_Stories_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_Integration_ProjectID",
                table: "Integrations",
                newName: "IX_Integrations_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_HelpCenter_ProjectID",
                table: "HelpCenters",
                newName: "IX_HelpCenters_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_FAQ_ProjectID",
                table: "FAQs",
                newName: "IX_FAQs_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_FAQ_HelpCenterID",
                table: "FAQs",
                newName: "IX_FAQs_HelpCenterID");

            migrationBuilder.RenameIndex(
                name: "IX_FAQ_FAQID1",
                table: "FAQs",
                newName: "IX_FAQs_FAQID1");

            migrationBuilder.RenameIndex(
                name: "IX_Comment_StoryID",
                table: "Comments",
                newName: "IX_Comments_StoryID");

            migrationBuilder.AlterColumn<long>(
                name: "TagID",
                table: "TagStories",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<long>(
                name: "StoryID",
                table: "TagStories",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<long>(
                name: "TagStoryID",
                table: "TagStories",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "TagID",
                table: "TagFAQs",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FAQID",
                table: "TagFAQs",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<long>(
                name: "TagFAQID",
                table: "TagFAQs",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "ProjectID",
                table: "Tags",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "TagID",
                table: "Tags",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "ProjectID",
                table: "Stories",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "StoryID",
                table: "Stories",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "ProjectID",
                table: "Projects",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "ProjectID",
                table: "Integrations",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "IntegrationID",
                table: "Integrations",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "ProjectID",
                table: "HelpCenters",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "HelpCenterID",
                table: "HelpCenters",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<long>(
                name: "ProjectID",
                table: "FAQs",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "HelpCenterID",
                table: "FAQs",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FAQID1",
                table: "FAQs",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FAQID",
                table: "FAQs",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<long>(
                name: "TagID",
                table: "FAQs",
                type: "bigint",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "StoryID",
                table: "Comments",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CommentID",
                table: "Comments",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TagStories",
                table: "TagStories",
                column: "TagStoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TagFAQs",
                table: "TagFAQs",
                column: "TagFAQID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tags",
                table: "Tags",
                column: "TagID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Stories",
                table: "Stories",
                column: "StoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projects",
                table: "Projects",
                column: "ProjectID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Integrations",
                table: "Integrations",
                column: "IntegrationID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HelpCenters",
                table: "HelpCenters",
                column: "HelpCenterID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FAQs",
                table: "FAQs",
                column: "FAQID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comments",
                table: "Comments",
                column: "CommentID");

            migrationBuilder.CreateIndex(
                name: "IX_FAQs_TagID",
                table: "FAQs",
                column: "TagID");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Stories_StoryID",
                table: "Comments",
                column: "StoryID",
                principalTable: "Stories",
                principalColumn: "StoryID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQs_FAQs_FAQID1",
                table: "FAQs",
                column: "FAQID1",
                principalTable: "FAQs",
                principalColumn: "FAQID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQs_HelpCenters_HelpCenterID",
                table: "FAQs",
                column: "HelpCenterID",
                principalTable: "HelpCenters",
                principalColumn: "HelpCenterID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQs_Projects_ProjectID",
                table: "FAQs",
                column: "ProjectID",
                principalTable: "Projects",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQs_Tags_TagID",
                table: "FAQs",
                column: "TagID",
                principalTable: "Tags",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HelpCenters_Projects_ProjectID",
                table: "HelpCenters",
                column: "ProjectID",
                principalTable: "Projects",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Integrations_Projects_ProjectID",
                table: "Integrations",
                column: "ProjectID",
                principalTable: "Projects",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Stories_Projects_ProjectID",
                table: "Stories",
                column: "ProjectID",
                principalTable: "Projects",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TagFAQs_FAQs_FAQID",
                table: "TagFAQs",
                column: "FAQID",
                principalTable: "FAQs",
                principalColumn: "FAQID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TagFAQs_Tags_TagID",
                table: "TagFAQs",
                column: "TagID",
                principalTable: "Tags",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tags_Projects_ProjectID",
                table: "Tags",
                column: "ProjectID",
                principalTable: "Projects",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TagStories_Stories_StoryID",
                table: "TagStories",
                column: "StoryID",
                principalTable: "Stories",
                principalColumn: "StoryID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TagStories_Tags_TagID",
                table: "TagStories",
                column: "TagID",
                principalTable: "Tags",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Stories_StoryID",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQs_FAQs_FAQID1",
                table: "FAQs");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQs_HelpCenters_HelpCenterID",
                table: "FAQs");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQs_Projects_ProjectID",
                table: "FAQs");

            migrationBuilder.DropForeignKey(
                name: "FK_FAQs_Tags_TagID",
                table: "FAQs");

            migrationBuilder.DropForeignKey(
                name: "FK_HelpCenters_Projects_ProjectID",
                table: "HelpCenters");

            migrationBuilder.DropForeignKey(
                name: "FK_Integrations_Projects_ProjectID",
                table: "Integrations");

            migrationBuilder.DropForeignKey(
                name: "FK_Stories_Projects_ProjectID",
                table: "Stories");

            migrationBuilder.DropForeignKey(
                name: "FK_TagFAQs_FAQs_FAQID",
                table: "TagFAQs");

            migrationBuilder.DropForeignKey(
                name: "FK_TagFAQs_Tags_TagID",
                table: "TagFAQs");

            migrationBuilder.DropForeignKey(
                name: "FK_Tags_Projects_ProjectID",
                table: "Tags");

            migrationBuilder.DropForeignKey(
                name: "FK_TagStories_Stories_StoryID",
                table: "TagStories");

            migrationBuilder.DropForeignKey(
                name: "FK_TagStories_Tags_TagID",
                table: "TagStories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TagStories",
                table: "TagStories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tags",
                table: "Tags");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TagFAQs",
                table: "TagFAQs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Stories",
                table: "Stories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projects",
                table: "Projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Integrations",
                table: "Integrations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HelpCenters",
                table: "HelpCenters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FAQs",
                table: "FAQs");

            migrationBuilder.DropIndex(
                name: "IX_FAQs_TagID",
                table: "FAQs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comments",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "TagID",
                table: "FAQs");

            migrationBuilder.RenameTable(
                name: "TagStories",
                newName: "TagStory");

            migrationBuilder.RenameTable(
                name: "Tags",
                newName: "Tag");

            migrationBuilder.RenameTable(
                name: "TagFAQs",
                newName: "TagFAQ");

            migrationBuilder.RenameTable(
                name: "Stories",
                newName: "Story");

            migrationBuilder.RenameTable(
                name: "Projects",
                newName: "Project");

            migrationBuilder.RenameTable(
                name: "Integrations",
                newName: "Integration");

            migrationBuilder.RenameTable(
                name: "HelpCenters",
                newName: "HelpCenter");

            migrationBuilder.RenameTable(
                name: "FAQs",
                newName: "FAQ");

            migrationBuilder.RenameTable(
                name: "Comments",
                newName: "Comment");

            migrationBuilder.RenameIndex(
                name: "IX_TagStories_TagID",
                table: "TagStory",
                newName: "IX_TagStory_TagID");

            migrationBuilder.RenameIndex(
                name: "IX_TagStories_StoryID",
                table: "TagStory",
                newName: "IX_TagStory_StoryID");

            migrationBuilder.RenameIndex(
                name: "IX_Tags_ProjectID",
                table: "Tag",
                newName: "IX_Tag_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_TagFAQs_TagID",
                table: "TagFAQ",
                newName: "IX_TagFAQ_TagID");

            migrationBuilder.RenameIndex(
                name: "IX_TagFAQs_FAQID",
                table: "TagFAQ",
                newName: "IX_TagFAQ_FAQID");

            migrationBuilder.RenameIndex(
                name: "IX_Stories_ProjectID",
                table: "Story",
                newName: "IX_Story_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_Integrations_ProjectID",
                table: "Integration",
                newName: "IX_Integration_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_HelpCenters_ProjectID",
                table: "HelpCenter",
                newName: "IX_HelpCenter_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_FAQs_ProjectID",
                table: "FAQ",
                newName: "IX_FAQ_ProjectID");

            migrationBuilder.RenameIndex(
                name: "IX_FAQs_HelpCenterID",
                table: "FAQ",
                newName: "IX_FAQ_HelpCenterID");

            migrationBuilder.RenameIndex(
                name: "IX_FAQs_FAQID1",
                table: "FAQ",
                newName: "IX_FAQ_FAQID1");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_StoryID",
                table: "Comment",
                newName: "IX_Comment_StoryID");

            migrationBuilder.AlterColumn<int>(
                name: "TagID",
                table: "TagStory",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "StoryID",
                table: "TagStory",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "TagStoryID",
                table: "TagStory",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectID",
                table: "Tag",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TagID",
                table: "Tag",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "TagID",
                table: "TagFAQ",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FAQID",
                table: "TagFAQ",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "TagFAQID",
                table: "TagFAQ",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectID",
                table: "Story",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "StoryID",
                table: "Story",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectID",
                table: "Project",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectID",
                table: "Integration",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IntegrationID",
                table: "Integration",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectID",
                table: "HelpCenter",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HelpCenterID",
                table: "HelpCenter",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectID",
                table: "FAQ",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HelpCenterID",
                table: "FAQ",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FAQID1",
                table: "FAQ",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FAQID",
                table: "FAQ",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "StoryID",
                table: "Comment",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CommentID",
                table: "Comment",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TagStory",
                table: "TagStory",
                column: "TagStoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tag",
                table: "Tag",
                column: "TagID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TagFAQ",
                table: "TagFAQ",
                column: "TagFAQID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Story",
                table: "Story",
                column: "StoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Project",
                table: "Project",
                column: "ProjectID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Integration",
                table: "Integration",
                column: "IntegrationID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HelpCenter",
                table: "HelpCenter",
                column: "HelpCenterID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FAQ",
                table: "FAQ",
                column: "FAQID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comment",
                table: "Comment",
                column: "CommentID");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Story_StoryID",
                table: "Comment",
                column: "StoryID",
                principalTable: "Story",
                principalColumn: "StoryID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQ_FAQ_FAQID1",
                table: "FAQ",
                column: "FAQID1",
                principalTable: "FAQ",
                principalColumn: "FAQID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQ_HelpCenter_HelpCenterID",
                table: "FAQ",
                column: "HelpCenterID",
                principalTable: "HelpCenter",
                principalColumn: "HelpCenterID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FAQ_Project_ProjectID",
                table: "FAQ",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HelpCenter_Project_ProjectID",
                table: "HelpCenter",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Integration_Project_ProjectID",
                table: "Integration",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Story_Project_ProjectID",
                table: "Story",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tag_Project_ProjectID",
                table: "Tag",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TagFAQ_FAQ_FAQID",
                table: "TagFAQ",
                column: "FAQID",
                principalTable: "FAQ",
                principalColumn: "FAQID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TagFAQ_Tag_TagID",
                table: "TagFAQ",
                column: "TagID",
                principalTable: "Tag",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TagStory_Story_StoryID",
                table: "TagStory",
                column: "StoryID",
                principalTable: "Story",
                principalColumn: "StoryID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TagStory_Tag_TagID",
                table: "TagStory",
                column: "TagID",
                principalTable: "Tag",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
