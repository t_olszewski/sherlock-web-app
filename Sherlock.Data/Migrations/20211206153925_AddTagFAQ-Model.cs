﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class AddTagFAQModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TagStory_Tag_TagID",
                table: "TagStory");

            migrationBuilder.CreateTable(
                name: "TagFAQ",
                columns: table => new
                {
                    TagFAQID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagID = table.Column<int>(type: "int", nullable: true),
                    FAQID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagFAQ", x => x.TagFAQID);
                    table.ForeignKey(
                        name: "FK_TagFAQ_FAQ_FAQID",
                        column: x => x.FAQID,
                        principalTable: "FAQ",
                        principalColumn: "FAQID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TagFAQ_Tag_TagID",
                        column: x => x.TagID,
                        principalTable: "Tag",
                        principalColumn: "TagID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TagFAQ_FAQID",
                table: "TagFAQ",
                column: "FAQID");

            migrationBuilder.CreateIndex(
                name: "IX_TagFAQ_TagID",
                table: "TagFAQ",
                column: "TagID");

            migrationBuilder.AddForeignKey(
                name: "FK_TagStory_Tag_TagID",
                table: "TagStory",
                column: "TagID",
                principalTable: "Tag",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TagStory_Tag_TagID",
                table: "TagStory");

            migrationBuilder.DropTable(
                name: "TagFAQ");

            migrationBuilder.AddForeignKey(
                name: "FK_TagStory_Tag_TagID",
                table: "TagStory",
                column: "TagID",
                principalTable: "Tag",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
