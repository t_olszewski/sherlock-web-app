﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class FAQModelAddProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectID",
                table: "FAQ",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FAQ_ProjectID",
                table: "FAQ",
                column: "ProjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_FAQ_Project_ProjectID",
                table: "FAQ",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FAQ_Project_ProjectID",
                table: "FAQ");

            migrationBuilder.DropIndex(
                name: "IX_FAQ_ProjectID",
                table: "FAQ");

            migrationBuilder.DropColumn(
                name: "ProjectID",
                table: "FAQ");
        }
    }
}
