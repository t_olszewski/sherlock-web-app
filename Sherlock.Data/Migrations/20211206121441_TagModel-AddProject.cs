﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class TagModelAddProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectID",
                table: "Tag",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tag_ProjectID",
                table: "Tag",
                column: "ProjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_Tag_Project_ProjectID",
                table: "Tag",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tag_Project_ProjectID",
                table: "Tag");

            migrationBuilder.DropIndex(
                name: "IX_Tag_ProjectID",
                table: "Tag");

            migrationBuilder.DropColumn(
                name: "ProjectID",
                table: "Tag");
        }
    }
}
