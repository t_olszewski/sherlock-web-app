﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sherlock.Migrations
{
    public partial class RemoveFAQTag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FAQs_Tags_TagID",
                table: "FAQs");

            migrationBuilder.DropTable(
                name: "TagFAQs");

            migrationBuilder.DropIndex(
                name: "IX_FAQs_TagID",
                table: "FAQs");

            migrationBuilder.DropColumn(
                name: "TagID",
                table: "FAQs");

            migrationBuilder.CreateTable(
                name: "FAQTag",
                columns: table => new
                {
                    FAQsFAQID = table.Column<long>(type: "bigint", nullable: false),
                    TagsTagID = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FAQTag", x => new { x.FAQsFAQID, x.TagsTagID });
                    table.ForeignKey(
                        name: "FK_FAQTag_FAQs_FAQsFAQID",
                        column: x => x.FAQsFAQID,
                        principalTable: "FAQs",
                        principalColumn: "FAQID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FAQTag_Tags_TagsTagID",
                        column: x => x.TagsTagID,
                        principalTable: "Tags",
                        principalColumn: "TagID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FAQTag_TagsTagID",
                table: "FAQTag",
                column: "TagsTagID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FAQTag");

            migrationBuilder.AddColumn<long>(
                name: "TagID",
                table: "FAQs",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TagFAQs",
                columns: table => new
                {
                    TagFAQID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FAQID = table.Column<long>(type: "bigint", nullable: false),
                    TagID = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagFAQs", x => x.TagFAQID);
                    table.ForeignKey(
                        name: "FK_TagFAQs_FAQs_FAQID",
                        column: x => x.FAQID,
                        principalTable: "FAQs",
                        principalColumn: "FAQID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TagFAQs_Tags_TagID",
                        column: x => x.TagID,
                        principalTable: "Tags",
                        principalColumn: "TagID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FAQs_TagID",
                table: "FAQs",
                column: "TagID");

            migrationBuilder.CreateIndex(
                name: "IX_TagFAQs_FAQID",
                table: "TagFAQs",
                column: "FAQID");

            migrationBuilder.CreateIndex(
                name: "IX_TagFAQs_TagID",
                table: "TagFAQs",
                column: "TagID");

            migrationBuilder.AddForeignKey(
                name: "FK_FAQs_Tags_TagID",
                table: "FAQs",
                column: "TagID",
                principalTable: "Tags",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
