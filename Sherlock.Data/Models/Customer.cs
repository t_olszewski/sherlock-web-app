﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class Customer
    {
        public long CustomerID { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "Customer Name cannot be longer than 80 characters.")]
        [Display(Name = "Customer Name:")]
        public string CustomerName { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "Contact Name cannot be longer than 80 characters.")]
        [Display(Name = "Contact Name:")]
        public string ContactName { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Contact Number:")]
        public string ContactNumber { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Contact Email:")]
        public string ContactEmail { get; set; }

        [Display(Name = "Members:")]
        public virtual ICollection<AppUser> Members { get; set; } = new List<AppUser>();

        [Display(Name = "Projects:")]
        public virtual ICollection<Project> Projects { get; set; } = new List<Project>();

    }
}