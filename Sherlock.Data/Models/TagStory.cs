﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class TagStory {
  
        public long TagStoryID { get; set; }

        [Required]
        public virtual Tag Tag { get; set; }

        [Required]
        public virtual Story Story { get; set; }

        [Required]
        public bool IsPrimary { get; set; }

    }
}