﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Sherlock.Data.Models
{
    public class AppUser : IdentityUser
    {
        public virtual ICollection<IdentityUserRole<string>> Roles { get; } = new List<IdentityUserRole<string>>();

        public string Name { get; set; }

        public string AccessToken { get; set; }

        public virtual ICollection<Project> Projects { get; set; } = new List<Project>();

        public virtual Customer Customer { get; set; }

    }
}
