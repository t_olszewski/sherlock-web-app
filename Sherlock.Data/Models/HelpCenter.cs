﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class HelpCenter
    {
        public long HelpCenterID { get; set; }

        public virtual Project Project { get; set; }

        [Display(Name = "Description:")]
        public string Description { get; set; }

        [Display(Name = "Top Qestions:")]
        public virtual ICollection<FAQ> TopQestions { get; set; } = new List<FAQ>();

    }
}