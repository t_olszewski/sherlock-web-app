﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class HelpCenterTopQuestions
    {
        public long HelpCenterTopQuestionID { get; set; }

        public virtual HelpCenter HelpCenter { get; set; }

        public virtual FAQ faq { get; set; }

    }
}