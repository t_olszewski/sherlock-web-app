﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public enum ProjectPlatform
    {
        iOS, Anadroid
    }

    public enum ProjectStatus
    {
        Active, Disabled, Expired
    }

    public class Project
    {
        public long ProjectID { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "App name cannot be longer than 80 characters.")]
        [Display(Name = "App Name:")]
        public string AppName { get; set; }

        [ReadOnly(true)]
        [Display(Name = "App Key:")]
        public long AppKey { get; set; }

        [Required]
        [Display(Name = "Platform:")]
        public ProjectPlatform Platform { get; set; }

        [Required]
        [Display(Name = "Status:")]
        public ProjectStatus Status { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Licence Start Date:")]
        public DateTime LicenceStartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Licence End Date:")]
        public DateTime LicenceEndDate { get; set; }

        [Display(Name = "Stories:")]
        public virtual ICollection<Story> Stories { get; set; } = new List<Story>();

        [Display(Name = "Integrations:")]
        public virtual ICollection<Integration> Integrations { get; set; } = new List<Integration>();


        public virtual AppUser AppUser { get; set; }
    }
}