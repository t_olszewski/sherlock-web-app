﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class Story
    {
        public long StoryID { get; set; }

        [Required]
        [Display(Name = "Name of story:")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description of story:")]
        public string Description { get; set; }

        [Display(Name = "Project:")]
        public virtual Project Project { get; set; }

        [Display(Name = "Tags:")]
        public virtual ICollection<TagStory> TagStories { get; set; } = new List<TagStory>();

        [Display(Name = "Comments:")]
        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

    }
}