﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class Tag
    {
        public long TagID { get; set; }

        [Required]
        [Display(Name = "Name of tag:")]
        public string Name { get; set; }

        [Display(Name = "Project:")]
        public virtual Project Project { get; set; }

        public virtual ICollection<TagStory> TagStories { get; set; } = new List<TagStory>();
        public virtual ICollection<FAQ> FAQs { get; set; } = new List<FAQ>();

    }
}