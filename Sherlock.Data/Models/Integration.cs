﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public enum IntegratorKinds
    {
        PivotalTracker, Jira, Zendesk
    }

    public class Integration
    {
        public long IntegrationID { get; set; }

        public virtual Project Project { get; set; }

        [Required]
        [Display(Name = "Integration with:")]
        public IntegratorKinds IntegratorKind { get; set; }

        [Required]
        [Display(Name = "Integrator URL Data")]
        public string URLData { get; set; }

        [Required]
        [Display(Name = "Integrator Authorization Data (multiple fields separated by ':' character):")]
        public string AuthorizationData { get; set; }

    }
}
