﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.Data.Models
{
    public class Comment
    {
        public long CommentID { get; set; }

        [Required]
        [Display(Name = "Comment:")]
        public string Text { get; set; }

        [Display(Name = "Story:")]
        public virtual Story Story { get; set; }

    }
}