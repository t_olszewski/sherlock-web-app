﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sherlock.Data.Helpers;

namespace Sherlock.Data.Models
{
    public enum FAQType
    {
        None, KnownIssue, SystemInfo
    }

    public enum FAQLinksToType
    {
        URL, Custom, NestedOptions
    }

    public class FAQ
    {
        public long FAQID { get; set; }

        public virtual Project Project { get; set; }

        [Required]
        public string Title { get; set; }

        public string Subtitle { get; set; }

        public FAQType Type { get; set; }

        [Display(Name = "Links to...")]
        public virtual FAQLinksToType LinksToType { get; set; }

        [Url]
        [RequiredIf("LinksToType", FAQLinksToType.URL)]
        public string URL { get; set; }

        [Display(Name = "HTML Content")]
        // [RequiredIf("LinksToType", FAQLinksToType.Custom)]
        public string HTMLContent { get; set; }

        [Display(Name = "Include Report A Problem link in solution")]
        public bool IncludeReportProblemLink { get; set; }

        [RequiredIf("LinksToType", FAQLinksToType.NestedOptions)]
        [Display(Name = "Nested options")]
        public virtual ICollection<FAQ> NestedOptions { get; set; } = new List<FAQ>();

        public virtual ICollection<Tag> Tags { get; set; } = new List<Tag>();

    }

}