﻿using Sherlock.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Sherlock.Data
{
    // This class is required to faciliate migrations (running `dotnet ef migrations ...`) because
    // our `DataContext` lives in a different project to the main WebHost
    // https://docs.microsoft.com/en-gb/ef/core/miscellaneous/cli/dbcontext-creation#from-a-design-time-factory
    // https://blog.tonysneed.com/2018/12/20/idesigntimedbcontextfactory-and-dependency-injection-a-love-story/
    internal class DataContextFactory : IDesignTimeDbContextFactory<SherlockContext>
    {
        public SherlockContext CreateDbContext(string[] args)
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (environment == null)
            {
                throw new Exception("No `ASPNETCORE_ENVIRONMENT` environment variable was set.  Because the `DataContext` is "
                    + "defined in a dedicated project, migrations are not environment aware and a 'ASPNETCORE_ENVIRONMENT' must be set. \n"
                    + "You probably want to run the same command again but prefix it with: \n"
                    + "    `export ASPNETCORE_ENVIRONMENT=Development; <command>`");
            }

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory() + "/../Sherlock")
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true)
                .AddJsonFile($"appsettings.Custom.json", optional: true)
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<SherlockContext>()
                .UseLazyLoadingProxies()
                .UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            return new SherlockContext(optionsBuilder.Options);
        }
    }

    public class SherlockContext : IdentityDbContext<AppUser>
    {
        public SherlockContext(DbContextOptions<SherlockContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Integration> Integrations { get; set; }
        public DbSet<Story> Stories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagStory> TagStories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<FAQ> FAQs { get; set; }
        public DbSet<HelpCenter> HelpCenters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>().HasMany(p => p.Roles).WithOne().HasForeignKey(p => p.UserId).IsRequired();

            /*
            modelBuilder
                .Entity<Project>()
                .HasMany(e => e.Stories)
                .WithOne(e => e.Project)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder
                .Entity<Story>()
                .HasMany(e => e.TagStories)
                .WithOne(e => e.Story)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder
               .Entity<Story>()
               .HasMany(e => e.Comments)
               .WithOne(e => e.Story)
               .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder
                .Entity<Tag>()
                .HasMany(e => e.TagStories)
                .WithOne(e => e.Tag)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder
                .Entity<FAQ>()
                .HasMany(e => e.Tags)
                .WithOne(e => e.FAQ)
                .OnDelete(DeleteBehavior.ClientCascade); */
        }
        
    }
}