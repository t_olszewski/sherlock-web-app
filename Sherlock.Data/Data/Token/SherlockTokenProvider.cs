﻿using System;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Sherlock.Data
{
    public class SherlockTokenProvider<TUser> : DataProtectorTokenProvider<TUser> where TUser : class
    {
        public SherlockTokenProvider(IDataProtectionProvider dataProtectionProvider, IOptions<SherlockTokenProviderOptions> options, ILogger<DataProtectorTokenProvider<TUser>> logger) : base(dataProtectionProvider, options, logger)
        {

        }
    }
}
