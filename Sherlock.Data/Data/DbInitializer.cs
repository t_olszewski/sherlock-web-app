﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Sherlock.Data.Models;

namespace Sherlock.Data
{
    public static class DbInitializer
    {
        public static async Task Initialize(SherlockContext context, UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (context.Projects.Any())
            {
                return;
            }

            IdentityRole adminRole = new IdentityRole("Admin");
            IdentityRole superadminRole = new IdentityRole("Superadmin");

            // create roles
            IdentityResult createAdminRole = await roleManager.CreateAsync(adminRole);
            IdentityResult createSuperadminRole = await roleManager.CreateAsync(superadminRole);

            // create admin account
            AppUser admin = new AppUser
            {
                Name = "Tomasz Olszewski",
                UserName = "to@compsoft.com", // Username is required by we are using email to authorization
                Email = "to@compsoft.com",
            };

            IdentityResult createUser = await userManager.CreateAsync(admin, "Secret123$");
            IdentityResult assignRoleToUser = await userManager.AddToRoleAsync(admin, "Admin");

            // AccessToken must be saved AFTER save user, otherwise, logging in with the access token will fail
            admin.AccessToken = await userManager.GenerateUserTokenAsync(admin, "Sherlock", "Sherlock");
            IdentityResult updateUser = await userManager.UpdateAsync(admin);

            // create superadmin account
            AppUser superadmin = new AppUser
            {
                Name = "Test User",
                UserName = "dp@compsoft.com", // Username is required by we are using email to authorization
                Email = "dp@compsoft.com"
            };

            IdentityResult createSuperUser = await userManager.CreateAsync(superadmin, "Compsoft123$");
            IdentityResult assignRoleToSuperUser = await userManager.AddToRoleAsync(superadmin, "Superadmin");

            // AccessToken must be saved AFTER save user, otherwise, logging in with the access token will fail
            superadmin.AccessToken = await userManager.GenerateUserTokenAsync(superadmin, "Sherlock", "Sherlock");
            IdentityResult updateSuperUser = await userManager.UpdateAsync(superadmin);

            // Add test PT project and integration

            var ptProject = new Project
            {
                 AppName = "PivotalTracker Test Project for iOS",
                 AppKey = RandomNumberGenerator.GetInt32(1, Int32.MaxValue),
                 Platform = ProjectPlatform.iOS,
                 Status = ProjectStatus.Active,
                 LicenceStartDate = DateTime.Today,
                 LicenceEndDate = DateTime.Today.AddMonths(1),
                 AppUser = admin
            };

            context.Projects.Add(ptProject);

            var ptIntegration = new Integration
            {
                Project = ptProject,
                IntegratorKind = IntegratorKinds.PivotalTracker,
                AuthorizationData = "338d6a6b1922795dab44abf25b232c3e",
                URLData = "2485714"
            };

            context.Integrations.Add(ptIntegration);

            // Add test Jira project and integration

            var jiraProject = new Project
            {
                AppName = "Jira Test Project for iOS",
                AppKey = RandomNumberGenerator.GetInt32(1, Int32.MaxValue),
                Platform = ProjectPlatform.iOS,
                Status = ProjectStatus.Active,
                LicenceStartDate = DateTime.Today,
                LicenceEndDate = DateTime.Today.AddMonths(1),
                AppUser = admin
            };

            context.Projects.Add(jiraProject);

            var jiraIntegration = new Integration
            {
                Project = jiraProject,
                IntegratorKind = IntegratorKinds.Jira,
                AuthorizationData = "demo:demo",
                URLData = "jira.demo.almworks.com"
            };

            context.Integrations.Add(jiraIntegration);

            // Add test ZenDesk project and integration

            var zendDeskProject = new Project
            {
                AppName = "ZenDesk Test Project for iOS",
                AppKey = RandomNumberGenerator.GetInt32(1, Int32.MaxValue),
                Platform = ProjectPlatform.iOS,
                Status = ProjectStatus.Active,
                LicenceStartDate = DateTime.Today,
                LicenceEndDate = DateTime.Today.AddMonths(1),
                AppUser = admin
            };

            context.Projects.Add(zendDeskProject);

            var zenDeskIntegration = new Integration
            {
                Project = zendDeskProject,
                IntegratorKind = IntegratorKinds.Zendesk,
                AuthorizationData = "to@compsoft.com:tnjUvAOxVjlBvICjUHSwd1alEp1leFi3x9Qp1bOj",
                URLData = "compsoft.zendesk.com",
            };

            context.Integrations.Add(zenDeskIntegration);

            // Add test customer

            var customer = new Customer
            {
                CustomerName = "Compsoft",
                ContactName = "Tomasz Olszewski",
                ContactNumber = "+48506688810",
                ContactEmail = "to@compsoft.com"
            };

            // Add users to customer

            customer.Members.Add(admin);

            // Add projects to customer

            customer.Projects.Add(ptProject);
            customer.Projects.Add(jiraProject);
            customer.Projects.Add(zendDeskProject);

            context.Customers.Add(customer);

            // Add test tags

            var tags = new Tag[]
            {
                new Tag { Name = "web", Project = ptProject },
                new Tag { Name = "demo", Project = ptProject },
                new Tag { Name = "supportbot",  Project = ptProject },
                new Tag { Name = "everytime",  Project = ptProject },
            };

            foreach (Tag tag in tags)
            {
                context.Tags.Add(tag);
            }

            // Add test FAQs

            var faq = new FAQ
            {
                Project = ptProject,
                Title = "Lorem ipsum dolor sit amet",
                Subtitle = "Lorem ipsum dolor sit amet consectetur adipisicing elit?",
                LinksToType = FAQLinksToType.URL,
                URL = "https://www.compsoft.co.uk/"
            };

            // Add tags to FAQs

            foreach (Tag tag in tags)
            {
                faq.Tags.Add(tag);
            }

            context.FAQs.Add(faq);

            // Add test help center

            var helpCenter = new HelpCenter
            {
                Description = "test HelpCenter for PivotalTracker Test Project",
                Project = ptProject
            };

            helpCenter.TopQestions.Add(faq);

            context.HelpCenters.Add(helpCenter);
            /*
            // Add test story

            var story = new Story { Name = "Test Story", Description = "Test Story Description", Project = project };

            var TagStories = new TagStory[]
            {
                new TagStory { Tag = tags[0], Story = story },
                new TagStory { Tag = tags[1], Story = story },
                new TagStory { Tag = tags[2], Story = story },
            };

            foreach (TagStory TagStory in TagStories)
            {
                context.TagStories.Add(TagStory);
            }


            var comment = new Comment { Text = "test comment" };
            var comment2nd = new Comment { Text = "2bd test comment :)" };
            story.Comments = new List<Comment>
            {
                comment, comment2nd
            };

            context.Stories.Add(story);
            */

            context.SaveChanges();
        }
    }
}