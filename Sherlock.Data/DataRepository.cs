﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Data.Models;

namespace Sherlock.Data
{
    public class DataRepository: IDataRepository
    {
        private readonly SherlockContext _context;

        public DataRepository(SherlockContext context)
        {
            _context = context;
        }

        // Projects

        public async Task<Project> Project(long id) => await _context.Projects.FindAsync(id);

        public async Task<string> ProjectAppName(long id) => await _context.Projects
                    .Where(p => p.ProjectID == id)
                    .Select(p => p.AppName)
                    .FirstOrDefaultAsync();

        public IEnumerable<Project> ProjectsDropDownList()
        {
            var projectsQuery = from p in _context.Projects
                                orderby p.AppName
                                select p;
            return projectsQuery.AsNoTracking();
        }

        // Stories

        public async Task<Story> Story(long id) => await _context.Stories.FindAsync(id);

        // FAQs

        public async Task<FAQ> FAQ(long id) => await _context.FAQs.FindAsync(id);

        // Tags

        public async Task<Tag> Tag(long id) => await _context.Tags.FindAsync(id);

        public IEnumerable<Tag> TagsDropDownList(long projectID)
        {
            var tagsQuery = from p in _context.Tags
                            where p.Project.ProjectID == projectID
                            orderby p.Name
                            select p;
            return tagsQuery.AsNoTracking();
        }

        // Integrations

        public async Task<Integration> Integration(long id) => await _context.Integrations.FindAsync(id);

        // Roles

        public IEnumerable<IdentityRole> RolesDropDownList()
        {
            var rolesQuery = from p in _context.Roles
                             orderby p.Name
                             select p;
            return rolesQuery.AsNoTracking();
        }
    }
}
