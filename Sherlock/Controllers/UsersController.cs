﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sherlock.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Sherlock.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Sherlock.ViewModels;
using Sherlock;

namespace CMS.Controllers
{
    [Authorize(Roles = "Superadmin")]
    public class UsersController : Controller
    {
        private readonly SherlockContext _context;
        private readonly IDataRepository _repository;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UsersController(SherlockContext context, IDataRepository repository, UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _repository = repository;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<ActionResult> IndexAsync(int? pageNumber)
        {
            int pageSize = 10;

            var usersWithRoles = (from user in _context.Users
                                  select new User()
                                  {
                                      UserId = user.Id,
                                      Email = user.Email,
                                      Role = string.Join(",", (from userRole in user.Roles
                                                   join role in _context.Roles on userRole.RoleId
                                                   equals role.Id
                                                   select role.Name).ToList())
                                  });

            return View(await PaginatedList<User>.CreateAsync(usersWithRoles.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<JsonResult> ShowAccessToken(string id)
        {
            var accessToken = await _context.Users
                .Where(u => u.Id == id)
                .Select(u => u.AccessToken)
                .FirstOrDefaultAsync();
            if (accessToken == null)
            {
                return Json(NotFound());
            }
            return new JsonResult(accessToken);
        }

        public IActionResult Create()
        {
            PopulateRolesDropDownList();
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Email,Password")] User user, string role)
        {
            AppUser appUser = new AppUser
            {
                UserName = user.Email, // Username is required by we are using email to authorization
                Email = user.Email
            };
            
            IdentityResult createUser = await _userManager.CreateAsync(appUser, user.Password);
            IdentityResult addRoleToUser = await _userManager.AddToRoleAsync(appUser, role);

            // AccessToken must be saved AFTER save user, otherwise, logging in with the access token will fail
            appUser.AccessToken = await _userManager.GenerateUserTokenAsync(appUser, "Sherlock", "Sherlock");
            IdentityResult updateUser = await _userManager.UpdateAsync(appUser);

            if (!IsValidResult(createUser) || !IsValidResult(createUser) || !IsValidResult(updateUser))
            {
                PopulateRolesDropDownList();
                return View();
            } else
            {
                return RedirectToAction("Index", "Users");
            }
        }

        // GET: Users/Delete/username
        public async Task<IActionResult> Delete(string email)
        {
            var user = await _context.Users.FirstOrDefaultAsync(m => m.Email == email);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string email)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool IsValidResult(IdentityResult result)
        {
            bool Succeeded = result.Succeeded;
            if (!Succeeded)
            {
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return Succeeded;
        }

        private void PopulateRolesDropDownList()
        {
            ViewBag.Role = new SelectList(_repository.RolesDropDownList(), "Name", "Name", null);
        }
    }

}