﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;
using Microsoft.AspNetCore.Http;

namespace Sherlock.Controllers
{
    [Authorize(Roles = "Admin,Superadmin")]
    public class ProjectsController : BaseController
    {
        public ProjectsController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        // GET: Projects
        public async Task<IActionResult> Index(int? pageNumber)
        {
            int pageSize = 10;

            var projects = _context.Projects
                .Include(p => p.Stories)
                .OrderBy(p => p.ProjectID);

            if (HttpContext.Session.GetString("Project") == null && HttpContext.Session.GetString("projectID") == null)
            {
                var project = await projects.FirstOrDefaultAsync();
                HttpContext.Session.SetString("Project", project.AppName);
                HttpContext.Session.SetString("projectID", project.ProjectID.ToString());
            }

            return View(await PaginatedList<Project>.CreateAsync(projects.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Projects/Switch/5
        public async Task<IActionResult> Switch(long? projectID)
        {
            if (projectID == null)
            {
                return NotFound();
            }

            Project project = await _repository.Project(projectID.Value);
            if (project == null)
            {
                return NotFound();
            }

            HttpContext.Session.SetString("Project", project.AppName);
            HttpContext.Session.SetString("projectID", project.ProjectID.ToString());
            return RedirectToAction(nameof(Index));
        }

        // GET: Projects/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Project project = await _repository.Project(id.Value);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }


        // GET: Projects/AddMonth/5
        public async Task<IActionResult> AddMonth(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Project project = await _repository.Project(id.Value);
            if (project == null)
            {
                return NotFound();
            }

            project.LicenceEndDate = project.LicenceEndDate.AddMonths(1);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> UpdateDate(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Project project = await _repository.Project(id.Value);
            if (project == null)
            {
                return NotFound();
            }

            project.LicenceEndDate = project.LicenceEndDate.AddMonths(1);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Projects/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Projects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProjectID,AppName,Platform,Status,LicenceStartDate,LicenceEndDate")] Project project)
        {
            if (ModelState.IsValid)
            {
                _context.Add(project);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(project);
        }

        // GET: Projects/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Project project = await _repository.Project(id.Value);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ProjectID,AppName,Platform,Status,LicenceStartDate,LicenceEndDate")] Project project)
        {
            if (id != project.ProjectID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(project);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectExists(project.ProjectID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(project);
        }

        // GET: Projects/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Project project = await _repository.Project(id.Value);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long projectID)
        {
            Project project = await _repository.Project(projectID);
            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProjectExists(long id)
        {
            return _context.Projects.Any(e => e.ProjectID == id);
        }

    }
}
