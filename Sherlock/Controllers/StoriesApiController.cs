﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sherlock.Data;
using Sherlock.Data.Models;
using Sherlock.ViewModels.API;
using Sherlock.ViewModels.Integrators;
using Sherlock.ViewModels;

namespace Sherlock.Controllers
{
    [Route("api/stories")]
    [ApiController]
    public class StoriesApiController : ControllerBase
    {
        private readonly SherlockContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public StoriesApiController(SherlockContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: api/stories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Story>>> GetStories()
        {
            return await _context.Stories.ToListAsync();
        }

        // GET: api/stories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Story>> GetStory(long id)
        {
            var story = await _context.Stories.FindAsync(id);

            if (story == null)
            {
                return NotFound();
            }

            return story;
        }

        // PUT: api/StoriesApi/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStory(long id, Story story)
        {
            if (id != story.StoryID)
            {
                return BadRequest();
            }

            _context.Entry(story).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/stories
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Story>> PostStory([FromHeader(Name = "Email")] string email,
                                                         [FromHeader(Name = "Token")] string token,
                                                         [FromBody] StoryApi storyApi)
        {
            if (!isAutorized(email, token).Result)
            {
                return Message(StatusCodes.Status401Unauthorized, "Authorization failed");
            }

            var story = new Story { Name = storyApi.Name, Description = storyApi.Description };

            Project project = _context
                    .Projects
                    .Include(p => p.Integrations)
                    .FirstOrDefault(p => p.ProjectID == storyApi.ProjectID);

            if (project == null)
            {
                return Message(StatusCodes.Status404NotFound, "Project not exist");
            }

            story.Project = project;

            for (int index = 0; index < storyApi.Tags.Count(); index++)
            {
                var tagApi = storyApi.Tags[index];

                var tag = _context
                    .Tags
                    .FirstOrDefault(t => t.Name == tagApi && t.Project == project);
                if (tag == null)
                {
                    tag = new Tag { Name = tagApi, Project = project };
                }

                var TagStory = new TagStory { Tag = tag, Story = story, IsPrimary = index == 0 };
                _context.TagStories.Add(TagStory);
            }

            // story.Comments = new List<Comment>(); // not required!
            foreach (string commentApi in storyApi.Comments)
            {
                var comment = new Comment { Text = commentApi };
                story.Comments.Add(comment);
            }
            _context.Stories.Add(story);
            await _context.SaveChangesAsync();

            var integration = project.Integrations.First();

            switch (integration.IntegratorKind)
            {
                case IntegratorKinds.PivotalTracker:
                    PivotalTrackerIntegrator integratorPT = new(integration.URLData, integration.AuthorizationData);
                    var responcePT = integratorPT.AddStory(storyApi);
                    return ParseIntegratoResponce(responcePT, story);
                case IntegratorKinds.Jira:
                    var authorizationJiraData = integration.AuthorizationData.Split(":");
                    if (authorizationJiraData.Count() != 2) {
                        return Message(StatusCodes.Status409Conflict, "Incorrect authorization data for Jira project");
                    }
                    JiraIntegrator integratorJira = new(integration.URLData, authorizationJiraData[0], authorizationJiraData[1]);
                    var responceJira = integratorJira.AddStory(storyApi);
                    return ParseIntegratoResponce(responceJira, story);
                case IntegratorKinds.Zendesk:
                    var authorizationZendeskData = integration.AuthorizationData.Split(":");
                    if (authorizationZendeskData.Count() != 2)
                    {
                        return Message(StatusCodes.Status409Conflict, "Incorrect authorization data for Zendesk project");
                    }
                    if (storyApi.Comments.Count() > 0)
                    {
                        return Message(StatusCodes.Status409Conflict, "For Zenddesk ticket, comments will be ignored");
                    }
                    ZenDeskIntegrator integratorZendesk = new(integration.URLData, authorizationZendeskData[0], authorizationZendeskData[1]);
                    var responceZendesk = integratorZendesk.AddStory(storyApi);
                    return ParseIntegratoResponce(responceZendesk, story);
            }

            return Message(StatusCodes.Status200OK, "Story created without Integrator");
        }

        private async Task<bool> isAutorized(string email, string token)
        {
            if (String.IsNullOrEmpty(email) || String.IsNullOrEmpty(token)){
                return false;
            }

            AppUser user = await _userManager.FindByEmailAsync(email);
            if (user == null) {
                return false;
            }

            bool result = await _userManager.VerifyUserTokenAsync(user, "Sherlock", "Sherlock", token);
            return result;
        } 

        private static JsonResult Message(int code, string response)
        {
            return new JsonResult(new IntegratorResponse()
            {
                StatusCode = code,
                Response = response
            });
        }

        private ActionResult<Story> ParseIntegratoResponce(IntegratorResponse responce, Story story)
        {
            return responce.StatusCode == StatusCodes.Status201Created
                ? CreatedAtAction("GetStory", new { id = story.StoryID }, story)
                : (ActionResult<Story>)CreatedAtAction("GetStory", new { id = story.StoryID }, responce);
        }

        // DELETE: api/stories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStory(long id)
        {
            var story = await _context.Stories.FindAsync(id);
            if (story == null)
            {
                return NotFound();
            }

            _context.Stories.Remove(story);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StoryExists(long id)
        {
            return _context.Stories.Any(e => e.StoryID == id);
        }

        // GET: api/stories/testPT
        [HttpGet("TestPT")]
        public JsonResult TestPT()
        {
            PivotalTrackerIntegrator integrator = new("2485714", "338d6a6b1922795dab44abf25b232c3e");

            var requestParameters = new StoryApi
            {
                Name = "test PT story",
                Description = "test description",
                Comments = new string[] { "test comment" },
                Tags = new string[] { "test label" },
            };
            var response = integrator.AddStory(requestParameters);
            return new JsonResult(response);
        }

        // GET: api/stories/testJira
        [HttpGet("TestJira")]
        public JsonResult TestJira()
        {
            JiraIntegrator integrator = new("jira.demo.almworks.com", "demo", "demo");

            var requestParameters = new StoryApi
            {
                Name = "test jira issue",
                Description = "test description",
                Comments = new string[] { "test comment" },
                Tags = new string[] { "test tag" },
            };
            var response = integrator.AddStory(requestParameters);
            return new JsonResult(response);
        }

        // GET: api/stories/testZenDesk
        [HttpGet("TestZenDesk")]
        public JsonResult TestZenDesk()
        {
            ZenDeskIntegrator integrator = new("compsoft.zendesk.com", "to@compsoft.com", "tnjUvAOxVjlBvICjUHSwd1alEp1leFi3x9Qp1bOj");

            var requestParameters = new StoryApi
            {
                Name = "test zendesk issue",
                Description = "test description",
                Comments = new string[] { "test comment" },
                Tags = new string[] { "test tag" },
            };
            var response = integrator.AddStory(requestParameters);
            return new JsonResult(response);
        }

    }
}