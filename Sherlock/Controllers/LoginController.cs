﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sherlock.Data.Models;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Sherlock.Data;
using Sherlock.ViewModels;

namespace CMS.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public LoginController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(Login login)
        {
            if (ModelState.IsValid)
            {
                AppUser user = await _userManager.FindByEmailAsync(login.Username);
                if (user != null)
                {
                    await _signInManager.SignOutAsync();
                    Microsoft.AspNetCore.Identity.SignInResult result = await _signInManager.PasswordSignInAsync(user, login.Password, login.RememberMe, false);

                    if (result.Succeeded)
                    {
                        return Redirect(login.ReturnUrl);
                    }
                }
            }
            ModelState.AddModelError("", "Invalid name or password");
            return View(login);
        }

        /*
        public async Task<IActionResult> Create()
        {
            // create roles
            IdentityResult createAdminRole = await _roleManager.CreateAsync(new IdentityRole(Roles.Admin.ToString()));
            IdentityResult createSuperadminRole = await _roleManager.CreateAsync(new IdentityRole(Roles.Superadmin.ToString()));
            if (!IsValidResult(createAdminRole) || !IsValidResult(createSuperadminRole))
            {
                return View();
            }
            
            // create admin account
            AppUser admin = new AppUser
            {
                UserName = "to@compsoft.com", // Username is required by we are using email to authorization
                Email = "to@compsoft.com"
            };
            
            IdentityResult createUser = await _userManager.CreateAsync(admin, "Secret123$");
            IdentityResult assignRoleToUser = await _userManager.AddToRoleAsync(admin, Roles.Admin.ToString());

            // AccessToken must be saved AFTER save user, otherwise, logging in with the access token will fail
            admin.AccessToken = await _userManager.GenerateUserTokenAsync(admin, "Sherlock", "Sherlock");
            IdentityResult updateUser = await _userManager.UpdateAsync(admin);

            if (!IsValidResult(createUser) || !IsValidResult(assignRoleToUser) || !IsValidResult(updateUser))
            {
                return View();
            }

            // create superadmin account
            AppUser superadmin = new AppUser
            {
                UserName = "dp@compsoft.com", // Username is required by we are using email to authorization
                Email = "dp@compsoft.com"
            };

            IdentityResult createSuperUser = await _userManager.CreateAsync(superadmin, "Compsoft123$");
            IdentityResult assignRoleToSuperUser = await _userManager.AddToRoleAsync(superadmin, Roles.Superadmin.ToString());

            // AccessToken must be saved AFTER save user, otherwise, logging in with the access token will fail
            superadmin.AccessToken = await _userManager.GenerateUserTokenAsync(superadmin, "Sherlock", "Sherlock");
            IdentityResult updateSuperUser = await _userManager.UpdateAsync(superadmin);

            if (!IsValidResult(createSuperUser) || !IsValidResult(assignRoleToSuperUser) || !IsValidResult(updateSuperUser))
            {
                return View();
            }

            return View();
        }
        */

        private bool IsValidResult(IdentityResult result) {
            bool Succeeded = result.Succeeded;
            if (!Succeeded) {
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return Succeeded;
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Login");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

    }
}