﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;

namespace Sherlock.Controllers
{
    public class IntegrationsController : BaseController
    {
        public IntegrationsController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        // GET: Integrations
        public async Task<IActionResult> Index(long? projectID, int? pageNumber)
        {
            if (projectID == null)
            {
                return NotFound();
            }

            int pageSize = 10;

            var integrations = _context.Integrations;

            await PopulateProjectDataAsync(projectID);

            return View(await PaginatedList<Integration>.CreateAsync(integrations.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Integrations/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Integration integration = await _repository.Integration(id.Value);
            if (integration == null)
            {
                return NotFound();
            }

            return View(integration);
        }

        // GET: Integrations/Create
        public async Task<IActionResult> CreateAsync(long? projectID)
        {
            await PopulateProjectDataAsync(projectID);
            return View();
        }

        // POST: Integrations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IntegrationID,IntegratorKind,URLData,AuthorizationData")] Integration integration, long projectID)
        {
            if (ModelState.IsValid)
            {
                Project project = await _repository.Project(projectID);
                if (project == null)
                {
                    return NotFound();
                }
                else
                {
                    integration.Project = project;
                }
                _context.Add(integration);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Integrations", new { projectID });
            }

            await PopulateProjectDataAsync(projectID);
            return View(integration);
        }

        // GET: Integrations/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Integration integration = await _repository.Integration(id.Value);
            if (integration == null)
            {
                return NotFound();
            }

            return View(integration);
        }

        // POST: Integrations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("IntegrationID,IntegratorKind,URLData,AuthorizationData")] Integration integration, long? projectID)
        {
            if (id != integration.IntegrationID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(integration);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!IntegrationExists(integration.IntegrationID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "Integrations", new { projectID });
            }

            await PopulateProjectDataAsync(projectID);
            return View(integration);
        }

        // GET: Integrations/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Integration integration = await _repository.Integration(id.Value);
            if (integration == null)
            {
                return NotFound();
            }

            return View(integration);
        }

        // POST: Integrations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            Integration integration = await _repository.Integration(id);
            var projectID = integration.Project.ProjectID;
            _context.Integrations.Remove(integration);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Integrations", new { projectID });
        }

        private bool IntegrationExists(long id)
        {
            return _context.Integrations.Any(e => e.IntegrationID == id);
        }

        private async Task PopulateProjectDataAsync(long? projectID)
        {
            ViewData["projectID"] = projectID;
            ViewData["ProjectName"] = await _repository.ProjectAppName(projectID.Value);
        }
    }
}
