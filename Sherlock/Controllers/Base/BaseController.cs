﻿using System;
using Microsoft.AspNetCore.Mvc;
using Sherlock.Data;

namespace Sherlock.Controllers.Base
{
    public class BaseController: Controller
    {
        protected readonly SherlockContext _context;
        protected readonly IDataRepository _repository;

        public BaseController(SherlockContext context, IDataRepository repository)
        {
            _context = context;
            _repository = repository;
        }
    }
}
