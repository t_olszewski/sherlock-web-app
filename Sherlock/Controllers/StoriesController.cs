﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;

namespace Sherlock.Controllers
{
    [Authorize(Roles = "Admin,Superadmin")]
    public class StoriesController : BaseController
    {
        public StoriesController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        // GET: Stories
        public async Task<IActionResult> Index(long? projectID, int? pageNumber)
        {
            if(projectID == null)
            {
                return NotFound();
            }

            int pageSize = 10;

            var projects = from s in _context.Stories
                           select s;

            if (projectID != null)
            {
                projects = projects.Where(s => s.Project.ProjectID == projectID);

                ViewData["projectID"] = projectID;
                ViewData["ProjectName"] = await _repository.ProjectAppName(projectID.Value);
            }

            projects = projects.Include(s => s.Project)
                    .Include(s => s.TagStories)
                        .ThenInclude(t => t.Tag)
                    .Include(s => s.Comments);

            return View(await PaginatedList<Story>.CreateAsync(projects.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Stories/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Story story = await _repository.Story(id.Value);
            if (story == null)
            {
                return NotFound();
            }

            return View(story);
        }

        // GET: Stories/Tags/5
        public async Task<IActionResult> Tags(long? id, int? pageNumber)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["LabelName"] = await _context.Tags
                    .Where(t => t.TagID == id)
                    .Select(l => l.Name).FirstOrDefaultAsync();
            ViewData["Id"] = id;

            int pageSize = 10;

            var TagStories = _context.TagStories
                    .Include(s => s.Story)
                    .Where(s => s.Tag.TagID == id);

            return View(await PaginatedList<TagStory>.CreateAsync(TagStories.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Stories/Create
        public IActionResult Create(long? id)
        {
            PopulateProjectsDropDownList();
            ViewData["id"] = id;
            return View();
        }

        // POST: Stories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StoryID,Project,Name,Description")] Story story, long projectID)
        {
            if (ModelState.IsValid)
            {
                Project project = await _repository.Project(projectID);
                if (project == null)
                {
                    return NotFound();
                } else {
                    story.Project = project;
                }
                _context.Add(story);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(story);
        }

        // GET: Stories/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Story story = await _repository.Story(id.Value);
            if (story == null)
            {
                return NotFound();
            }

            return View(story);
        }

        // POST: Stories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("StoryID,Name,Description")] Story story)
        {
            if (id != story.StoryID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(story);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StoryExists(story.StoryID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(story);
        }

        // GET: Stories/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Story story = await _repository.Story(id.Value);
            if (story == null)
            {
                return NotFound();
            }

            return View(story);
        }

        // POST: Stories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            Story story = await _repository.Story(id);
            _context.Stories.Remove(story);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // POST: Stories/AddTag/5/tag
        [HttpPost]
        public async Task<IActionResult> AddTag(long id, string tag)
        {
            Story story = await _repository.Story(id);
            if (story == null)
            {
                return NotFound();
            }

            var tagObject = await _context
                    .Tags
                    .FirstOrDefaultAsync(t => t.Name == tag && t.Project == story.Project );

            if (tagObject == null)
            {
                tagObject = new Tag { Name = tag, Project = story.Project };
            }

            var TagStory = new TagStory { Tag = tagObject, Story = story, IsPrimary = false };
            _context.TagStories.Add(TagStory);

            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Stories", new { id });
        }

        // GET: Stories/SetPrimaryTag/5
        public async Task<IActionResult> SetPrimaryTag(long? id, long? projectID)
        {
            var TagStory = await _context.TagStories.FindAsync(id);

            if (TagStory == null)
            {
                return NotFound();
            }

            await _context.TagStories
                .Where(ls => ls.Story == TagStory.Story)
                .ForEachAsync(ls => ls.IsPrimary = false);

            await _context.SaveChangesAsync();

            TagStory.IsPrimary = true;
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Stories", new { projectID });
        }

        // GET: Stories/DeleteTag/5
        public async Task<IActionResult> DeleteTag(long? id, long? projectID, long? storyID)
        {
            TagStory tagStory = await _context.TagStories.FindAsync(id);

            if (tagStory == null)
            {
                return NotFound();
            }

            if (tagStory.IsPrimary)
            {
                TempData["message"] = "You cannot remove the tag marked as primary";
            }
            else
            {
                _context.TagStories.Remove(tagStory);
                await _context.SaveChangesAsync();
            }
            
            if (projectID != null) {
                return RedirectToAction("Index", "Stories", new { projectID });
            } else {
                return RedirectToAction("Details", "Stories", new { id = storyID });
            }
        }

        // POST: Stories/AddComment/5/comment
        [HttpPost]
        public async Task<IActionResult> AddComment(long id, string text)
        {
            Story story = await _repository.Story(id);
            if (story == null)
            {
                return NotFound();
            }

            Comment comment = new Comment { Text = text };
            story.Comments.Add(comment);

            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Stories", new { id });
        }

        // GET: Stories/DeleteComment/5
        public async Task<IActionResult> DeleteComment(long? id, long? commentID)
        {
            Comment comment = await _context.Comments.FindAsync(commentID);

            if (comment == null)
            {
                return NotFound();
            }

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Stories", new { id });
        }

        private bool StoryExists(long id)
        {
            return _context.Stories.Any(e => e.StoryID == id);
        }

        private void PopulateProjectsDropDownList(object selectedProject = null)
        {
            ViewBag.projectID = new SelectList(_repository.ProjectsDropDownList(), "ProjectID", "Name", selectedProject);
        }
    }
}
