﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;

namespace Sherlock.Controllers
{
    public class TagsController : BaseController
    {
        public TagsController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        // GET: Tags
        public async Task<IActionResult> Index(long? projectID, int? pageNumber)
        {
            if (projectID == null)
            {
                return NotFound();
            }

            int pageSize = 10;

            var tags = _context
                .Tags
                .Include(f => f.Project)
                .Where(f => f.Project.ProjectID == projectID);

            ViewData["projectID"] = projectID;
            ViewData["ProjectName"] = await _repository.ProjectAppName(projectID.Value);

            return View(await PaginatedList<Tag>.CreateAsync(tags.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Tags/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tag tag = await _repository.Tag(id.Value);
            if (tag == null)
            {
                return NotFound();
            }

            return View(tag);
        }

        // GET: Tags/Create
        public async Task<IActionResult> CreateAsync(long? projectID)
        {
            ViewData["projectID"] = projectID;
            ViewData["ProjectName"] = await _repository.ProjectAppName(projectID.Value);
            return View();
        }

        // POST: Tags/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TagID,Name")] Tag tag, long projectID)
        {
            if (ModelState.IsValid)
            {
                Project project = await _repository.Project(projectID);
                if (project == null)
                {
                    return NotFound();
                }
                else
                {
                    tag.Project = project;
                }

                _context.Add(tag);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Tags", new { projectID = tag.Project.ProjectID });
            }
            return View(tag);
        }

        // GET: Tags/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tag tag = await _repository.Tag(id.Value);
            if (tag == null)
            {
                return NotFound();
            }
            return View(tag);
        }

        // POST: Tags/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("TagID,Name")] Tag tag, long? projectID)
        {
            if (id != tag.TagID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tag);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TagExists(tag.TagID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "Tags", new { projectID });
            }
            return View(tag);
        }

        // GET: Tags/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tag tag = await _repository.Tag(id.Value);
            if (tag == null)
            {
                return NotFound();
            }

            return View(tag);
        }

        // POST: Tags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            Tag tag = await _repository.Tag(id);
            var projectID = tag.Project.ProjectID;
            _context.Tags.Remove(tag);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Tags", new { projectID });
        }

        private bool TagExists(long id)
        {
            return _context.Tags.Any(e => e.TagID == id);
        }
    }
}
