﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;

namespace Sherlock.Controllers
{
    public class HelpCenterController : BaseController
    {
        public HelpCenterController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        // GET: HelpCenter
        public async Task<IActionResult> Index(long? projectID, int? pageNumber)
        {
            if (projectID == null)
            {
                return NotFound();
            }

            int pageSize = 10;

            var helpCenters = _context
                .HelpCenters
                .Include(f => f.Project)
                .Where(f => f.Project.ProjectID == projectID);

            return View(await PaginatedList<HelpCenter>.CreateAsync(helpCenters.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: HelpCenter/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helpCenter = await _context.HelpCenters.FindAsync(id);
            if (helpCenter == null)
            {
                return NotFound();
            }

            return View(helpCenter);
        }

        // GET: HelpCenter/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HelpCenter/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HelpCenterID,Description")] HelpCenter helpCenter)
        {
            if (ModelState.IsValid)
            {
                _context.Add(helpCenter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(helpCenter);
        }

        // GET: HelpCenter/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helpCenter = await _context.HelpCenters.FindAsync(id);
            if (helpCenter == null)
            {
                return NotFound();
            }
            return View(helpCenter);
        }

        // POST: HelpCenter/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("HelpCenterID,Description")] HelpCenter helpCenter)
        {
            if (id != helpCenter.HelpCenterID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(helpCenter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HelpCenterExists(helpCenter.HelpCenterID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(helpCenter);
        }

        // GET: HelpCenter/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HelpCenter helpCenter = await _context.HelpCenters.FindAsync(id);
            if (helpCenter == null)
            {
                return NotFound();
            }

            return View(helpCenter);
        }

        // POST: HelpCenter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var helpCenter = await _context.HelpCenters.FindAsync(id);
            _context.HelpCenters.Remove(helpCenter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HelpCenterExists(long id)
        {
            return _context.HelpCenters.Any(e => e.HelpCenterID == id);
        }
    }
}
