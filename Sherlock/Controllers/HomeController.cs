﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;
using Sherlock.ViewModels;

namespace Sherlock.Controllers
{
    [Authorize(Roles = "Admin,Superadmin")]
    public class HomeController : BaseController
    {
        public HomeController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
