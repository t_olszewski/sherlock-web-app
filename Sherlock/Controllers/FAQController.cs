﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sherlock.Controllers.Base;
using Sherlock.Data;
using Sherlock.Data.Models;

namespace Sherlock.Controllers
{
    public class FAQController : BaseController
    {
        public FAQController(SherlockContext context, IDataRepository repository) : base(context, repository) { }

        // GET: FAQ
        public async Task<IActionResult> Index(long? projectID, int? pageNumber)
        {
            if (projectID == null)
            {
                return NotFound();
            }

            int pageSize = 10;

            var faqWithTags = _context
                .FAQs
                .Include(f => f.Tags)
                .Include(f => f.Project)
                .Where(f => f.Project.ProjectID == projectID);

            PopulateTagsDropDownList(projectID.Value);
            await PopulateProjectDataAsync(projectID.Value);

            return View(await PaginatedList<FAQ>.CreateAsync(faqWithTags.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: FAQ/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FAQ fAQ = await _repository.FAQ(id.Value);
            if (fAQ == null)
            {
                return NotFound();
            }

            return View(fAQ);
        }

        // GET: FAQ/Create
        public async Task<IActionResult> CreateAsync(long? projectID)
        {
            await PopulateProjectDataAsync(projectID.Value);
            return View();
        }

        // POST: FAQ/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FAQID,Title,Subtitle,Type,LinksToType,URL,IncludeReportProblemLink")] FAQ fAQ, long projectID)
        {
            if (ModelState.IsValid)
            {
                Project project = await _repository.Project(projectID);
                if (project == null)
                {
                    return NotFound();
                }
                else
                {
                    fAQ.Project = project;
                }
                _context.Add(fAQ);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "FAQ", new { projectID });
            }

            await PopulateProjectDataAsync(projectID);
            return View(fAQ);
        }

        // GET: FAQ/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FAQ fAQ = await _repository.FAQ(id.Value);

            if (fAQ == null)
            {
                return NotFound();
            }

            PopulateTagsDropDownList(fAQ.Project.ProjectID);
            await PopulateProjectDataAsync(fAQ.Project.ProjectID);
            return View(fAQ);
        }

        // POST: FAQ/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("FAQID,Title,Subtitle,Type,LinksToType,URL,IncludeReportProblemLink")] FAQ fAQ, long? projectID)
        {
            if (id != fAQ.FAQID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fAQ);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FAQExists(fAQ.FAQID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "FAQ", new { projectID });
            }

            await PopulateProjectDataAsync(projectID.Value);
            return View(fAQ);
        }

        // GET: FAQ/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FAQ fAQ = await _repository.FAQ(id.Value);
            if (fAQ == null)
            {
                return NotFound();
            }

            return View(fAQ);
        }

        // POST: FAQ/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            FAQ fAQ = await _repository.FAQ(id);
            var projectID = fAQ.Project.ProjectID;
            _context.FAQs.Remove(fAQ);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "FAQ", new { projectID });
        }

        // POST: FAQ/AssignTag
        [HttpPost]
        public async Task<IActionResult> AssignTag(long FAQID, long tagID)
        {
            FAQ fAQ = await _repository.FAQ(FAQID);
            Tag tag = await _context.Tags.FindAsync(tagID);

            if (fAQ == null || tag == null)
            {
                return NotFound();
            }

            
            // check position currently exist
            if (!fAQ.Tags.Any(e => e.Name == tag.Name))
            {
                fAQ.Tags.Add(tag);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index", "FAQ", new { projectID = fAQ.Project.ProjectID });
        }

        // GET: FAQ/RemoveTag
        [HttpGet]
        public async Task<IActionResult> RemoveTag(long faqID, long tagID)
        {
            FAQ fAQ = await _repository.FAQ(faqID);
            Tag tag = await _context.Tags.FindAsync(tagID);

            if (fAQ == null || tag == null)
            {
                return NotFound();
            }

            fAQ.Tags.Remove(tag);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "FAQ", new { projectID = fAQ.Project.ProjectID });
        }

        private void PopulateTagsDropDownList(long projectID)
        {
            ViewBag.Tags = new SelectList(_repository.TagsDropDownList(projectID), "TagID", "Name");
        }

        private async Task PopulateProjectDataAsync(long projectID)
        {
            ViewData["projectID"] = projectID;
            ViewData["ProjectName"] = await _repository.ProjectAppName(projectID);
        }

        private bool FAQExists(long id)
        {
            return _context.FAQs.Any(e => e.FAQID == id);
        }
    }
}
