﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.ViewModels
{
    [NotMapped]
    public class User
    {
        public string UserId { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "E-mail:")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password:")]
        [UIHint("password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Role:")]
        public string Role { get; set; }

        public string AccessToken { get; set; }
    }
}
