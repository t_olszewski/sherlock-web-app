﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using RestSharp.Authenticators;
using Atlassian;
using Atlassian.Jira;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Sherlock.ViewModels.Integrators;
using Sherlock.ViewModels.API;

namespace Sherlock.ViewModels.Integrators
{
    public class JiraIntegrator: IntegratorInterface
    {
        private readonly string _url;
        private readonly string _username;
        private readonly string _password;

        public JiraIntegrator(string url, string username, string password)
        {

            _url = "https://" + url;
            _username = username;
            _password = password;
        }

        public IntegratorResponse AddStory(StoryApi requestParameters)
        {
            try
            {
                // Working only with RestSharp 106.11.7
                var jira = Jira.CreateRestClient(_url, _username, _password);

                var issue = jira.CreateIssue("QA");
                issue.Type = "Bug";
                issue.Priority = "Major";
                issue.Summary = requestParameters.Name;
                issue.Description = requestParameters.Description;
                // issue.CustomFields.AddArray("Custom Labels Field", "label1", "label2"); // error: Could not find custom field with name 'Custom Labels Field' on the JIRA server.

                issue.SaveChanges();

                return new IntegratorResponse()
                {
                    StatusCode = StatusCodes.Status201Created,
                    Response = issue.Status.ToString()
                };
            } catch (Exception e)
            {
                Console.WriteLine($"The file could not be opened: '{e}'");

                return new IntegratorResponse()
                {
                    StatusCode = StatusCodes.Status500InternalServerError,
                    Response = e.ToString()
                };
            }

        }

    }
}
