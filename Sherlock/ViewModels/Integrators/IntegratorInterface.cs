﻿using System;
using Sherlock.Data.Models;
using Sherlock.ViewModels.API;
using Sherlock.ViewModels.Integrators;

namespace Sherlock.ViewModels.Integrators
{

    interface IntegratorInterface
    {
        public IntegratorResponse AddStory(StoryApi requestParameters);
    }

}