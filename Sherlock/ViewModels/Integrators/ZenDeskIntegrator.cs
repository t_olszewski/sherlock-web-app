﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Sherlock.ViewModels.API;
using Sherlock.ViewModels.Integrators;
using ZendeskApi_v2;
using ZendeskApi_v2.Models.Tickets;

namespace Sherlock.ViewModels.Integrators
{
    public class ZenDeskIntegrator: IntegratorInterface
    {
        private readonly string _url;
        private readonly string _username;
        private readonly string _token;

        public ZenDeskIntegrator(string url, string username, string token)
        {
            _url = url;
            _username = username;
            _token = token;
        }

        public IntegratorResponse AddStory(StoryApi requestParameters)
        {
            var zenDesk = new ZendeskApi(_url, _username, _token, "en-us");

            Ticket ticket = new Ticket();
            ticket.Subject = requestParameters.Name;
            ZendeskApi_v2.Models.Tickets.Comment commentZendesk = new ZendeskApi_v2.Models.Tickets.Comment();
            commentZendesk.Body = requestParameters.Description;
            ticket.Comment = commentZendesk;

            ticket.Priority = "urgent";
            ticket.Tags = requestParameters.Tags;

            var task = zenDesk.Tickets.CreateTicket(ticket);
            task.ToString();

            return new IntegratorResponse()
            {
                StatusCode = StatusCodes.Status201Created,
                Response = task.Ticket.Url
            };

        }
    }
}
