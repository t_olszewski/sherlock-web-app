﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using Sherlock.ViewModels.API;
using Sherlock.ViewModels.Integrators;

namespace Sherlock.ViewModels.Integrators
{
    public class PivotalTrackerIntegrator : IntegratorInterface
    {
        private readonly string _url;
        private readonly string _token;

        public PivotalTrackerIntegrator(string projectID, string token)
        {
            _url = "https://www.pivotaltracker.com/services/v5/projects/" + projectID + "/stories";
            _token = token;
        }

        public IntegratorResponse AddStory(StoryApi requestParameters)
        {
            var client = new RestClient(_url);

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("X-TrackerToken", _token);

            request.AddParameter("name", requestParameters.Name);
            request.AddParameter("description", requestParameters.Description);
            request.AddParameter("story_type", "bug");
            foreach (string comment in requestParameters.Comments)
            {
                request.AddParameter("comments[][text]", comment);
            }
            foreach (string label in requestParameters.Tags)
            {
                request.AddParameter("labels[][name]", label);
            }

            IRestResponse response = client.Execute(request);
            return new IntegratorResponse()
            {
                StatusCode = (int)response.StatusCode,
                Response = response.Content
            };

        }

    }
}
