﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.ViewModels
{
    [NotMapped]
    public class Login
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email:")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Password:")]
        [UIHint("password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; } = "/";
    }
}
