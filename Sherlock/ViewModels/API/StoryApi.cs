﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.ViewModels.API
{
    [NotMapped]
    public class StoryApi
    {
        [Required]
        [Display(Name = "Name of story")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description of story")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Project ID")]
        public int ProjectID { get; set; }

        [Required]
        [Display(Name = "Tags")]
        public string[] Tags { get; set; }

        [Required]
        [Display(Name = "Comments")]
        public string[] Comments { get; set; }

    }
}