﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sherlock.ViewModels.API
{
    [NotMapped]
    public class IntegratorResponse
    {
        public int? StatusCode { get; set; }
#nullable enable
        public string? Status { get; set; }
#nullable disable
        public string Response { get; set; }
    }
}